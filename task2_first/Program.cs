﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Diagnostics;

namespace task2_first
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = Assembly.GetExecutingAssembly().Location;
            string secondName = Path.GetFileName(a) == "task2_first.exe" ? "task2_second.exe" : "task2_first.exe";
            string b = a.Replace(Path.GetFileName(a), secondName);            

            while (true)
            {
                Thread.Sleep(10000);

                if (!File.Exists(b))
                {
                    File.Copy(a, b);
                }

                if (!Process.GetProcessesByName(secondName.Substring(0, secondName.Length - 4)).Any())
                {
                    Process.Start(b);
                }
            }
        }
    }
}
