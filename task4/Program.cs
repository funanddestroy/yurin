﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using System.Data.SQLite;
using System.Data;

namespace task4
{
    public class Pair<T, U>
    {
        public Pair()
        {
        }

        public Pair(T first, U second)
        {
            this.First = first;
            this.Second = second;
        }

        public T First { get; set; }
        public U Second { get; set; }
    };

    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6);

            if (args.Length > 1)
            {
                Console.WriteLine("Много аргументов :)");
                return;
            }
            else if (args.Length == 1)
            {
                path = args[0];
            }

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Некорректный путь!");
                return;
            }

            Console.Write("1 - просканировать\n2 - проверить\n(1/2): ");
            int action = Console.Read() - '0';

            SQLiteConnection sqlite;
            SQLiteCommand command;
            string sql = "";

            switch (action)
            {
                case 1:
                    File.Delete("files.sqlite");

                    SQLiteConnection.CreateFile("files.sqlite");

                    using (sqlite = new SQLiteConnection("Data Source=files.sqlite"))
                    {
                        sqlite.Open();
                        sql = "create table files" +
                                    "(" +
                                       "path varchar(300)," +
                                       "name varchar(300)," +
                                       "size int" +
                                    ")";
                        command = new SQLiteCommand(sql, sqlite);
                        command.ExecuteNonQuery();
                        string[] files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);
                        foreach (string f in files)
                        {
                            FileInfo file = new FileInfo(f);
                            sql = "insert into files (path, name, size)" +
                                    "values ('" + file.DirectoryName + "', '" + file.Name + "', " + file.Length + ")";
                            command = new SQLiteCommand(sql, sqlite);
                            command.ExecuteNonQuery();
                        }

                        sqlite.Close();
                    }

                    break;

                case 2:
                    using (sqlite = new SQLiteConnection("Data Source=files.sqlite"))
                    {
                        sqlite.Open();
                        string[] files = Directory.GetFiles(path, "*", SearchOption.AllDirectories);

                        sql = "select * from files";
                        command = new SQLiteCommand(sql, sqlite);
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            List<Pair<string, int>> fls = new List<Pair<string, int>>();
                            while (reader.Read())
                            {
                                string pstr = reader["path"] + "\\" + reader["name"];
                                int size = int.Parse(reader["size"].ToString());
                                fls.Add(new Pair<string, int>(pstr, size));

                                if (!File.Exists(pstr))
                                {
                                    Console.WriteLine(">  Файла " + pstr + " нет =(");
                                    continue;
                                }

                                FileInfo fi = new FileInfo(pstr);
                                if (fi.Length != size)
                                {
                                    Console.WriteLine(">  Файл " + pstr + " был изменен");
                                    continue;
                                }
                                Console.WriteLine("Файл " + pstr + " в порядке");
                            }

                            bool flag = true;
                            foreach (string f in files)
                            {
                                foreach (Pair<string, int> p in fls)
                                {
                                    if (f == p.First)
                                    {
                                        flag = false;
                                        break;
                                    }
                                    flag = true;
                                }

                                if (flag)
                                {
                                    Console.WriteLine(">  Файл " + f + " отсутствует в базе");
                                }
                            }
                        }

                        sqlite.Close();
                    }

                    break;

                default:
                    Console.WriteLine("Введен неверный параметр");
                    break;
            }
        }
    }
}
