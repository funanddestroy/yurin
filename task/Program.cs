﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Diagnostics;

namespace task
{
    class Program
    {

        static void Main(string[] args)
        {
            FileInfo fileInfo = new FileInfo(Assembly.GetExecutingAssembly().Location);

            File.Delete(fileInfo.FullName + ".old");

            byte[] fileBytes = File.ReadAllBytes(fileInfo.FullName);

            MD5 md5 = new MD5CryptoServiceProvider();            

            int cnt = 50;
            byte[] fb = new byte[cnt];
            int j = 0;
            for (int i = fileBytes.Length - cnt; i < fileBytes.Length; i++)
            {
                fb[j] = fileBytes[i];
                j++;
            }

            string[] str = Encoding.UTF8.GetString(fb).Split(new char[] { '|' });

            if (str.Length > 1 && str[str.Length - 2] == "protect")
            {
                int fbl = fileBytes.Length - 33;
                byte[] checkSum = new byte[fbl];

                for (int i = 0; i < fbl; i++)
                {
                    checkSum[i] = fileBytes[i];
                }

                checkSum = md5.ComputeHash(checkSum);

                string cs = "";
                foreach (byte b in checkSum)
                {
                    cs += b.ToString("x2");
                }

                int n = Convert.ToInt32(str[str.Length - 3]);
                byte[] fbn = new byte[n];
                int j1 = 0;
                for (int i = fileBytes.Length - n; i < fileBytes.Length; i++)
                {
                    fbn[j1] = fileBytes[i];
                    j1++;
                }

                string[] strn = Encoding.UTF8.GetString(fbn).Split(new char[] { '|' });

                Console.WriteLine("Размер файла:\t\t\t\t" + fileInfo.Length);
                Console.WriteLine("Сохраненный размер файла:\t\t" + strn[strn.Length - 4]);
                Console.WriteLine("Совпадение:\t\t" + (fileInfo.Length.ToString() == strn[strn.Length - 4]));
                Console.WriteLine();
                Console.WriteLine("Имя файла:\t\t\t\t" + fileInfo.Name);
                Console.WriteLine("Сохраненное имя файла:\t\t\t" + strn[strn.Length - 7]);
                Console.WriteLine("Совпадение:\t\t" + (fileInfo.Name == strn[strn.Length - 7]));
                Console.WriteLine();
                Console.WriteLine("Путь к файлу:\t\t\t\t" + fileInfo.DirectoryName);
                Console.WriteLine("Сохраненный путь к файлу:\t\t" + strn[strn.Length - 6]);
                Console.WriteLine("Совпадение:\t\t" + (fileInfo.DirectoryName == strn[strn.Length - 6]));
                Console.WriteLine();
                string d = fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3);
                Console.WriteLine("Дата создания файла:\t\t\t" + d);
                Console.WriteLine("Сохраненная дата создания файла:\t" + strn[strn.Length - 5]);
                Console.WriteLine("Совпадение:\t\t" + (d == strn[strn.Length - 5]));
                Console.WriteLine();
                Console.WriteLine("Хэш файла:\t\t\t\t" + cs);
                Console.WriteLine("Сохраненный хэш файла:\t\t\t" + strn[strn.Length - 1]);
                Console.WriteLine("Совпадение:\t\t" + (cs == strn[strn.Length - 1]));
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Имя файла: " + fileInfo.Name);
                Console.WriteLine("Путь к файлу: " + fileInfo.DirectoryName);
                Console.WriteLine("Дата создания: " + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3));
                Console.WriteLine("Размер в байтах: " + fileBytes.Length);                

                File.Move(fileInfo.FullName, fileInfo.FullName + ".old");

                string data = "|" + fileInfo.Name
                    + "|" + fileInfo.DirectoryName
                    + "|" + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3)
                    + "|0000|000|protect";

                byte[] endata = Encoding.UTF8.GetBytes(data);

                data = "|" + fileInfo.Name
                    + "|" + fileInfo.DirectoryName
                    + "|" + fileInfo.CreationTimeUtc.ToString().Substring(0, fileInfo.CreationTimeUtc.ToString().Length - 3)
                    + "|" + (fileBytes.Length + endata.Length + 33) 
                    + "|" + (endata.Length + 33) 
                    + "|protect";
                endata = Encoding.UTF8.GetBytes(data);
                
                byte[] retBytes = new byte[fileBytes.Length + endata.Length];

                for (int i = 0; i < fileBytes.Length; i++)
                {
                    retBytes[i] = fileBytes[i];
                }

                int j1 = 0;
                for (int i = fileBytes.Length; i < fileBytes.Length + endata.Length; i++)
                {
                    retBytes[i] = endata[j1];
                    j1++;
                }

                byte[] checkSum = md5.ComputeHash(retBytes);

                string cs = "";
                foreach (byte b in checkSum)
                {
                    cs += b.ToString("x2");
                }

                Console.WriteLine("Хэш: " + cs);

                string s = "|" + cs;
                endata = Encoding.UTF8.GetBytes(s);

                BinaryWriter STG = new BinaryWriter(File.Open(fileInfo.FullName, FileMode.Create));
                foreach (byte b in retBytes)
                {
                    STG.Write(b);
                }

                foreach (byte b in endata)
                {
                    STG.Write(b);
                }

                STG.Close();

                Process.Start(fileInfo.FullName);
            }
        }
    }
}
